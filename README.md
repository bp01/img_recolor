# img_recolor

- [Image Recolor](#org2d933d6)
  - [Configuration file](#orgd187eef)
  - [Installation](#orga540e20)
  - [Usage](#org71cf377)
  - [Modes](#org83ba5dd)
    - [range](#orgb0ca3b5)
    - [channel](#org8ba0387)
    - [static](#orgc580428)
    - [dynamic](#org189e2f6)
    - [auto](#org702243e)



<a id="org2d933d6"></a>

# Image Recolor

A simple command line utility to recolor images based on custom colorschemes inspired by [ImageGoNord](https://github.com/Schrodinger-Hat/ImageGoNord). This program was designed as a counterpart to [Pywal](https://github.com/dylanaraps/pywal) to adjust the wallpaper according to the color palette.

*ImageGoNord*

[image](file:///Users/admin/wallpaper.png) *Image-Recolor*

There are several modes to choose between to achieve the best results. The settings may need to be adjusted if the result does not meet the expectations. Better results might be achieved if certain colors are changed in the configuration file (palette.yml). &#x2014;


<a id="orgd187eef"></a>

## Configuration file

PALETTE-NAME.yml *(Example Theme: [Kanagawa](https://github.com/rebelot/kanagawa.nvim))*

```yml
light_red: "#C34043"
dark_red: "#43242B"
light_green: "#76946A"
dark_green: "#2B3328"
light_blue: "#7FB4CA"
dark_blue: "#658594"
light_yellow: "#DCA561"
dark_yellow: "#49443C"
light_orange: "#F47066"
dark_orange: "#dda36E"
light_mauve: "#957FB8"
dark_mauve: "#988BA2"
light_pink: "#D27E99"
dark_pink: "#D15D75"
light_gray: "#DCD7BA"
dark_gray: "#18171e"
```

**dark colors and light colors other than red, green and blue are optional for channel mode**


<a id="orga540e20"></a>

## Installation

The project can be build for every supported architecture by using a recent version of cargo

```bash
git clone
cd recolor/
cargo build release
cd target/release/
```


<a id="org71cf377"></a>

## Usage

currently only PNG and JPEG files are supported due to limitations of the rust library [image::ImageBuffer](https://docs.rs/image/0.19.0/image/struct.ImageBuffer.html#method.save)

```bash
recolor path_to_base_image.png output_path.png path_to_config_file.yml mode {additional flags} `
```


<a id="org83ba5dd"></a>

## Modes


<a id="orgb0ca3b5"></a>

### range

*Replaces the colors with their closest base color defined in the config file. The sensitivity can be adjusted.* (**Best for cartoon like images**)

```bash
./recolor img.png out.png palette.yml range 10
```


<a id="org8ba0387"></a>

### channel

*Adjusts the color channels relative to the red, green and blue colors in the configuration file* (**Best for complex images and color gradients**)


<a id="orgc580428"></a>

### static

*replaces colors as a pixel wise operation (**recomended**)*

```bash
./recolor img.png out.png palette.yml channel static
```


<a id="org189e2f6"></a>

### dynamic

*calculates color intensity based on the general brightness and boosts contrast (**experimental**)*

```bash
./recolor img.png out.png palette.yml channel dynamic
```


<a id="org702243e"></a>

### auto

*determines the best mode automatically*

```bash
./recolor img.png out.png palette.yml auto
```
