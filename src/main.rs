use colors_transform::{Color, Rgb};
use image::GenericImageView;
use image::ImageBuffer;
use image::Pixel;
use image::Rgba;
use std::env;
use std::fs;
use std::process::exit;
use yaml_rust::YamlLoader;

fn hex_to_rgb(hex: &str, alpha: u8) -> [u8; 4] {
    let rgb = Rgb::from_hex_str(hex).unwrap();
    [
        rgb.get_red() as u8,
        rgb.get_green() as u8,
        rgb.get_blue() as u8,
        alpha,
    ]
}

fn hex_to_rgb_type(arr: [u8; 4]) -> Rgba<u8> {
    return *Rgba::from_slice(&[arr[0], arr[1], arr[2], arr[3]]);
}

fn yml_to_hex(yml: String) -> [String; 16] {
    let docs = YamlLoader::load_from_str(&yml).unwrap();
    let doc = &docs[0];
    return [
        doc["light_red"].as_str().unwrap().to_string(),
        doc["dark_red"].as_str().unwrap().to_string(),
        doc["light_green"].as_str().unwrap().to_string(),
        doc["dark_green"].as_str().unwrap().to_string(),
        doc["light_blue"].as_str().unwrap().to_string(),
        doc["dark_blue"].as_str().unwrap().to_string(),
        doc["light_yellow"].as_str().unwrap().to_string(),
        doc["dark_yellow"].as_str().unwrap().to_string(),
        doc["light_orange"].as_str().unwrap().to_string(),
        doc["dark_orange"].as_str().unwrap().to_string(),
        doc["light_mauve"].as_str().unwrap().to_string(),
        doc["dark_mauve"].as_str().unwrap().to_string(),
        doc["light_pink"].as_str().unwrap().to_string(),
        doc["dark_pink"].as_str().unwrap().to_string(),
        doc["light_gray"].as_str().unwrap().to_string(),
        doc["dark_gray"].as_str().unwrap().to_string(),
    ];
}

fn yml_to_rgb_hex(yml: String) -> [String; 3] {
    let docs = YamlLoader::load_from_str(&yml).unwrap();
    let doc = &docs[0];
    return [
        doc["light_red"].as_str().unwrap().to_string(),
        doc["light_green"].as_str().unwrap().to_string(),
        doc["light_blue"].as_str().unwrap().to_string(),
    ];
}

fn base_rgb_additive(pixel: Rgba<u8>, hex: Vec<String>, opt: bool) -> Rgba<u8> {
    let rgb = pixel.channels();

    let r: f64 =
    if rgb[0] == 0 {
        1.0
    } else {
        (rgb[0]) as f64
    };
    let g: f64 =
    if rgb[1] == 0 {
        1.0
    } else {
        (rgb[1]) as f64
    };
    let b: f64 =
    if rgb[2] == 0 {
        1.0
    } else {
        (rgb[2]) as f64
    };

    let percentage = vec![
        (r * 100.0) / 255.0,
        (g * 100.0) / 255.0,
        (b * 100.0) / 255.0,
    ];

    let p_sum: f64 = percentage.iter().sum();

    let rel_percentage = vec![
        (percentage[0] * 100.0) / p_sum,
        (percentage[1] * 100.0) / p_sum,
        (percentage[2] * 100.0) / p_sum,
    ];

    let bred = hex_to_rgb(&hex[0], 0);
    let bgreen = hex_to_rgb(&hex[1], 0);
    let bblue = hex_to_rgb(&hex[2], 0);
    let red = vec![bred[0], bred[1], bred[2]];
    let green = vec![bgreen[0], bgreen[1], bgreen[2]];
    let blue = vec![bblue[0], bblue[1], bblue[2]];

    let r_f_col = vec![
        (percentage[0] * red[0] as f64) / 100.0,
        (percentage[0] * red[1] as f64) / 100.0,
        (percentage[0] * red[2] as f64) / 100.0,
    ];

    let g_f_col = vec![
        (percentage[1] * green[0] as f64) / 100.0,
        (percentage[1] * green[1] as f64) / 100.0,
        (percentage[1] * green[2] as f64) / 100.0,
    ];

    let b_f_col = vec![
        (percentage[2] * blue[0] as f64) / 100.0,
        (percentage[2] * blue[1] as f64) / 100.0,
        (percentage[2] * blue[2] as f64) / 100.0,
    ];

    let a_mod = ((r * 0.2126) + (g * 1.7152) + (b * 0.0722)) / 255.0;
    let rel_switch = 1.0;

    let res: Rgba<u8> =
    if opt {
        *Rgba::from_slice(&[
            (r_f_col[0]
                * (rel_percentage[0] / 100.0)
                * (1.0
                    * (rel_switch
                        * ((rel_percentage[0] * a_mod) / (percentage[0] / rel_percentage[0])
                            * 0.1)))
                + g_f_col[0]
                    * (rel_percentage[1] / 100.0)
                    * (1.0
                        * (rel_switch
                            * ((rel_percentage[1] * a_mod) / (percentage[1] / rel_percentage[1])
                                * 0.1)))
                + b_f_col[0]
                    * (rel_percentage[2] / 100.0)
                    * (1.0
                        * (rel_switch
                            * ((rel_percentage[2] * a_mod) / (percentage[2] / rel_percentage[2])
                                * 0.1)))) as u8,
            (r_f_col[1]
                * (rel_percentage[0] / 100.0)
                * (1.0
                    * (rel_switch
                        * ((rel_percentage[0] * a_mod) / (percentage[0] / rel_percentage[0])
                            * 0.1)))
                + g_f_col[1]
                    * (rel_percentage[1] / 100.0)
                    * (1.0
                        * (rel_switch
                            * ((rel_percentage[1] * a_mod) / (percentage[1] / rel_percentage[1])
                                * 0.1)))
                + b_f_col[1]
                    * (rel_percentage[2] / 100.0)
                    * (1.0
                        * (rel_switch
                            * ((rel_percentage[2] * a_mod) / (percentage[2] / rel_percentage[2])
                                * 0.1)))) as u8,
            (r_f_col[2]
                * (rel_percentage[0] / 100.0)
                * (1.0
                    * (rel_switch
                        * ((rel_percentage[0] * a_mod) / (percentage[0] / rel_percentage[0])
                            * 0.1)))
                + g_f_col[2]
                    * (rel_percentage[1] / 100.0)
                    * (1.0
                        * (rel_switch
                            * ((rel_percentage[1] * a_mod) / (percentage[1] / rel_percentage[1])
                                * 0.1)))
                + b_f_col[2]
                    * (rel_percentage[2] / 100.0)
                    * (1.0
                        * (rel_switch
                            * ((rel_percentage[2] * a_mod) / (percentage[2] / rel_percentage[2])
                                * 0.1)))) as u8,
            255,
        ])
    } else {
        *Rgba::from_slice(&[
            ((r_f_col[0] * (rel_percentage[0] / 100.0)
                + g_f_col[0] * (rel_percentage[1] / 100.0)
                + b_f_col[0] * (rel_percentage[2] / 100.0))
                * 2.0) as u8,
            ((r_f_col[1] * (rel_percentage[0] / 100.0)
                + g_f_col[1] * (rel_percentage[1] / 100.0)
                + b_f_col[1] * (rel_percentage[2] / 100.0))
                * 2.0) as u8,
            ((r_f_col[2] * (rel_percentage[0] / 100.0)
                + g_f_col[2] * (rel_percentage[1] / 100.0)
                + b_f_col[2] * (rel_percentage[2] / 100.0))
                * 2.0) as u8,
            255,
        ])
    };
    res
}

fn get_rgb_ranges(hex: Vec<String>) -> [Rgba<u8>; 16] {
    return [
        *Rgba::from_slice(&hex_to_rgb(&hex[0], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[1], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[2], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[3], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[4], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[5], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[6], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[7], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[8], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[9], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[10], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[11], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[12], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[13], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[14], 255)),
        *Rgba::from_slice(&hex_to_rgb(&hex[15], 255)),
    ];
}

fn rgba_insert_alpha(pixel: Rgba<u8>, alpha: u8) -> Rgba<u8> {
    let channels = pixel.channels();
    return *Rgba::from_slice(&[channels[0], channels[1], channels[2], alpha]);
}

/*enum Colorspace {
    Red,
    Green,
    Blue,
    Yellow,
    Magenta,
    Pink,
    White,
    Black,
}*/

fn replace_in_range(
    pixel: Rgba<u8>,
    base: Rgba<u8>,
    new_color: Rgba<u8>,
    sensitivity: u8,
) -> Rgba<u8> {
    let pix_c = pixel.channels();
    let base_c = base.channels();
    let modi = ((pix_c[0] as f64 * 0.2126)
        + (pix_c[1] as f64 * 1.7152)
        + (pix_c[2] as f64 * 0.0722)) as u8;

    if ((pix_c[0] >= (base_c[0] - sensitivity)) && (pix_c[0] <= (base_c[0] + sensitivity)))
        && ((pix_c[1] >= (base_c[1] - sensitivity)) && (pix_c[1] <= (base_c[1] + sensitivity)))
        && ((pix_c[2] >= (base_c[2] - sensitivity)) && (pix_c[2] <= (base_c[2] + sensitivity)))
    {
        return *Rgba::from_slice(&[
            ((modi as f64 * new_color[0] as f64) / 33_f64) as u8,
            ((modi as f64 * new_color[1] as f64) / 33_f64) as u8,
            ((modi as f64 * new_color[2] as f64) / 33_f64) as u8,
            255,
        ]);
    } else {
        pixel
    }
}

fn get_hue(pixel: Rgba<u8>, hex: [Rgba<u8>; 16], c_mod: i64) -> Rgba<u8> {
    let rgb = pixel.channels();
    let r = rgb[0] as f64;
    let g = rgb[1] as f64;
    let b = rgb[2] as f64;

    let modi = ((r * 0.2126) + (g * 1.7152) + (b * 0.0722)) as u8;

    let light_red: Rgba<u8> = rgba_insert_alpha(hex[0], modi);
    let dark_red: Rgba<u8> = rgba_insert_alpha(hex[1], modi);
    let light_green: Rgba<u8> = rgba_insert_alpha(hex[2], modi);
    let dark_green: Rgba<u8> = rgba_insert_alpha(hex[3], modi);
    let light_blue: Rgba<u8> = rgba_insert_alpha(hex[4], modi);
    let dark_blue: Rgba<u8> = rgba_insert_alpha(hex[5], modi);
    let light_yellow: Rgba<u8> = rgba_insert_alpha(hex[6], modi);
    let dark_yellow: Rgba<u8> = rgba_insert_alpha(hex[7], modi);
    let light_orange: Rgba<u8> = rgba_insert_alpha(hex[8], modi);
    let dark_orange: Rgba<u8> = rgba_insert_alpha(hex[9], modi);
    let light_mauve: Rgba<u8> = rgba_insert_alpha(hex[10], modi);
    let dark_mauve: Rgba<u8> = rgba_insert_alpha(hex[11], modi);
    let light_pink: Rgba<u8> = rgba_insert_alpha(hex[12], modi);
    let dark_pink: Rgba<u8> = rgba_insert_alpha(hex[13], modi);
    let light_gray: Rgba<u8> = rgba_insert_alpha(hex[14], modi);
    let dark_gray: Rgba<u8> = rgba_insert_alpha(hex[15], modi);

    let is_dark: bool = (r * 0.2126) + (g * 0.7152) + (b * 0.0722) < (255 / 2) as f64 ;

    let range = c_mod;
    let da = rgb[2] as i64;
    let db = rgb[1] as i64;
    let dc = rgb[0] as i64;

    if (dc + db) / 2 + range >= da && (dc + db) / 2 - range <= da {
        if is_dark {
            dark_gray
        } else {
            light_gray
        }
    } else {
        if r > g && r > b {
            if g > b && g <= 145.0 {
                if is_dark {
                    return dark_orange;
                } else {
                    return light_orange;
                }
            } else if g > b && g > 145.0 {
                if is_dark {
                    return dark_yellow;
                } else {
                    return light_yellow;
                }
            } else if b > g && b <= 100.0 {
                if is_dark {
                    return dark_pink;
                } else {
                    return light_pink;
                }
            } else if is_dark {
                    return dark_red;
                } else {
                    return light_red;
                }
        }
        if g > b && g > r {
            if is_dark {
                dark_green
            } else {
                light_green
            }
        } else if r > g {
                if is_dark {
                    dark_mauve
                } else {
                    light_mauve
                }
            } else {
                if is_dark {
                    dark_blue
                } else {
                    light_blue
                }
            }
    }
}

fn det_auto_opt(
    image: image::DynamicImage,
    width: u32,
    height: u32,
    hex: [Rgba<u8>; 16],
    c_mod: i64,
) -> (bool, bool) {
    let mut cmp_pix: Rgba<u8> = *Rgba::from_slice(&[0, 0, 0, 0]);
    let mut cmp_ctr: i64 = 0;
    let mut buffer: f64 = 0.0;
    //let (width, height) = image.dimensions();
    for (_x, _y, pixel) in image.pixels() {
        let channels = pixel.channels();
        buffer += ((channels[0] as f64 * 0.2126)
            + (channels[1] as f64 * 0.7152)
            + (channels[2] as f64 * 0.0722)) as f64;
        let npix = get_hue(pixel, hex, c_mod);
        let nchannels = npix.channels();
        let cmp_channels = cmp_pix.channels();
        if nchannels[0] == cmp_channels[0]
            && nchannels[1] == cmp_channels[1]
            && nchannels[2] == cmp_channels[2]
        {
            cmp_ctr += 1;
        } else {
            cmp_ctr -= 1;
        }
        cmp_pix = npix;
    }
    buffer /= (width * height) as f64;
    let res: bool = buffer >= (255 / 2) as f64;
    let complexity_mod: f64 = 1.2;
    let complexity: bool = cmp_ctr as f64 >= ((width * height) as f64) / complexity_mod;
    //println!("Cmp_ctr: {}", cmp_ctr);
    //println!("Pix_sum: {}", (width*height));
    //println!("needed: {}", (width*height) as f64/complexity_mod);
    (complexity, res)
}

fn ranged(
    colorspace: String,
    args: Vec<String>,
) -> ImageBuffer<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>> {
    let image = image::open(&args[1]).unwrap();
    let (width, height) = image.dimensions();

    let hexvals: [String; 16] = yml_to_hex(colorspace);
    let vals: Vec<String> = hexvals.to_vec();

    let mut out = ImageBuffer::<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>>::new(width, height);

    let c_mod: i64 =
    if args.len() > 5 {
        args[5].parse::<i64>().unwrap()
    } else {
        -1
    };

    for (x, y, pixel) in image.pixels() {
        let p = get_hue(pixel, get_rgb_ranges(vals.to_vec()), c_mod);
        out.put_pixel(x, y, p);
    }
    out
}

fn channeled(
    colorspace: String,
    args: Vec<String>,
) -> ImageBuffer<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>> {
    let image = image::open(&args[1]).unwrap();
    let (width, height) = image.dimensions();

    let hexvals: [String; 3] = yml_to_rgb_hex(colorspace);
    let vals: Vec<String> = vec![
        hexvals[0].to_string(),
        hexvals[1].to_string(),
        hexvals[2].to_string(),
    ];

    let mut out = ImageBuffer::<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>>::new(width, height);
    if &args[5] == "dynamic" {
        for (x, y, pixel) in image.pixels() {
            out.put_pixel(x, y, base_rgb_additive(pixel, vals.to_vec(), true));
        }
    } else if &args[5] == "static" {
        for (x, y, pixel) in image.pixels() {
            out.put_pixel(x, y, base_rgb_additive(pixel, vals.to_vec(), false));
        }
    } else {
        println!("Please specify an operation! (dynamic || static)");
        exit(0);
    }
    out
}

fn replaced(
    base: Rgba<u8>,
    new_color: Rgba<u8>,
    sensitivity: u8,
    args: Vec<String>,
) -> ImageBuffer<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>> {
    let image = image::open(&args[1]).unwrap();
    let (width, height) = image.dimensions();

    let mut out = ImageBuffer::<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>>::new(width, height);
    for (x, y, pixel) in image.pixels() {
        out.put_pixel(x, y, replace_in_range(pixel, base, new_color, sensitivity));
    }
    out
}

fn rasterize(args: Vec<String>) -> ImageBuffer<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>> {
    let image = image::open(&args[1]).unwrap();
    let (width, height) = image.dimensions();

    let mut out = ImageBuffer::<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>>::new(width, height);

    let mut offset: u32;
    //let mut pixbuf:Rgba<u8> = *Rgba::from_slice(&[0, 0, 0, 255]);
    let mut r_x = 0;
    let mut r_y = 0;
    for (i, (x, y, pixel)) in (0_u32..).zip(image.pixels()) {
    //for (x, y, pixel) in image.pixels() {
        if y % 2 == 0 {
            offset = 0;
        } else {
            offset = 1;
        }
        if (i + offset) % 2 == 0 {
            out.put_pixel(r_x, r_y, pixel);
            //pixbuf = pixel;
        } else {
            //out.put_pixel(x, y, *Rgba::from_slice(&[0, 0, 0, 255]));
            //out.put_pixel(x, y, pixbuf);

            if (x  as i64 - 1) > 0 {
                r_x = x - 1;
            } else {
                r_x = x;
            }
            r_y = y;
        }
        //i += 1;
    }
    out
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let out: ImageBuffer<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>>;

    let colorspace = fs::read_to_string(&args[3]).unwrap();

    let opt: String = args[4].to_string();

    if opt == "range" {
        out = ranged(colorspace, args.to_vec());
    } else if opt == "auto" {
        let image = image::open(&args[1]).unwrap();
        let (width, height) = image.dimensions();
        let hexvals: [String; 16] = yml_to_hex(colorspace.to_string());
        let vals: Vec<String> = hexvals.to_vec();
        let t: (bool, bool) = det_auto_opt(image, width, height, get_rgb_ranges(vals.to_vec()), 0);
        let mut buffer: ImageBuffer<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>> =
            ImageBuffer::<Rgba<u8>, Vec<<Rgba<u8> as Pixel>::Subpixel>>::new(0, 0);

        if t == (true, true) {
            let mut argv = args.to_vec();
            argv[5] = "dynamic".to_string();
            buffer = channeled(colorspace, argv.to_vec());
        } else if t == (false, false) {
            let mut argv = args.to_vec();
            argv[5] = "10".to_string();
            buffer = ranged(colorspace, argv.to_vec());
        } else if t == (true, false) {
            let mut argv = args.to_vec();
            argv[5] = "static".to_string();
            buffer = channeled(colorspace, argv.to_vec());
        } else if t == (false, true) {
            let mut argv = args.to_vec();
            argv[5] = "0".to_string();
            buffer = ranged(colorspace, argv.to_vec());
        }
        out = buffer;
    } else if opt == "channel" {
        out = channeled(colorspace, args.to_vec());
    } else if opt == "replace" {
        out = replaced(
            hex_to_rgb_type(hex_to_rgb(&args[5], 255)),
            hex_to_rgb_type(hex_to_rgb(&args[6], 255)),
            args[7].parse::<u8>().unwrap(),
            args.to_vec(),
        );
    } else if opt == "rasterize" {
        out = rasterize(args.to_vec());
    } else {
        println!("Please specify an operation! (channel || range)");
        exit(0);
    }
    out.save(&args[2]).unwrap();
}
